"""
Store and retrieve data from the data base.

"""
import pandas as pd
from datetime import datetime, timedelta
import pymongo
import signac
import logging
import collections
logger = logging.getLogger("foldingdb.{}".format(__name__));

host='glotzerdb';
dbname = 'folding';
collections = [
    'testing',
    # 'structure',
    # 'pathway',
    # 'yield'
]

def isiterable(obj):
    return isinstance(obj, collections.Iterable)

def test_connection():
    connected = True;
    db = signac.get_database(name=dbname, hostname=host);
    # TODO: list collections and find one from each.
    return connected;

def upload(docs, collection, ordered=True):
    """
    http://api.mongodb.com/python/current/api/pymongo/collection.html
    """
    db = signac.get_database(name=dbname, hostname=host);
    result = None
    if isinstance(docs, dict):
        result = db[collection].insert_one(docs);
    elif isiterable(docs):
        result = db[collection].insert_many(docs, ordered)
    else:
        logger.error("Could not insert document(s).")
    # TODO: handle insert errors.
    return result;

# def retrieve_latest(account,  limit=1):
#     """
#     Retrive the last database entry based on the time and account
#     """
#     db = signac.get_database(name=dbname, hostname=host);
#     for doc in db[collection].find(filter={'account':account}, limit=limit, modifiers={'$orderby':{'time': pymongo.DESCENDING}}):
#         yield doc

def find_entries(collection, filter=None):
    """
    Retrive all entries associated with the account
    """
    db = signac.get_database(name=dbname, hostname=host);
    for doc in db[collection].find(filter=filter):
        yield doc

def collection_stats(account, collection, sort=False, limit=0):
    db = signac.get_database(name=dbname, hostname=host);
    stats = db.command("collstats", collection, scale=1024*1024)
    logger.warning("Reporting information for {ns}".format(ns=stats['ns']))
    logger.warning("entries found: {entries}\ntotal size: {sz:.1f} MB\naverage size: {asz:.1f} KB".format(entries=stats['count'], sz=stats['size'], asz=1024*stats['size']/stats['count']))

def list_all(collection, fmtstr=None):
    colect = get_collection(collection)
    for doc in colect.find():
        if fmtstr is None:
            logger.info(doc)
        else:
            logger.info(fmtstr.format(**doc))



def purge_entries(collection):
    pass;

def schema(**kwargs):
    return dict(**kwargs);

def get_database():
    return signac.get_database(name=dbname, hostname=host);

def get_collection(collection):
    db = get_database();
    assert collection in collections
    return db[collection]
